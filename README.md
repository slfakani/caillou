# TP 1 Javapro - Caillou

[![pipeline status](https://gitlab.isima.fr/slfakani/caillou/badges/master/pipeline.svg)](https://gitlab.isima.fr/slfakani/caillou/-/commits/master)
[![coverage report](https://gitlab.isima.fr/slfakani/caillou/badges/master/coverage.svg)](https://gitlab.isima.fr/slfakani/caillou/-/commits/master)

#### Travail de Slimane FAKANI et Dorian GRAVEGEAL

Caillou est une API REST qui permet à nos clients de se renseigner sur les produits qu'ils achètent.

Features :
* Calcul du nutriscore basé sur les informations nutritionnelles d'un produit
* Calcul de la classe du produit
* Gestion du panier
* Liste des additifs présents

## Choix techniques

### 1 - Règles métiers et paramétrage

Lors de la phase d'analyse du problème, nous sommes partis du principes que les règles métiers (calcul du nutriscore, classes et couleurs d'un produit, etc...) peuvent évoluer régulièrement. 
Nous avons abandonné l'idée de ne pas coder en dur les règles métiers car cela poserait des problèmes de maintenabilité en cas d'évolutions de ces règles.
Nous avons donc décidé de rendre paramétrable les règles de calcul du nutriscore et des classes de produits. Nous avons imiginé et testé plusieurs méthodes pour stocker ces règles :
* Fichier JSON : Un fichier de configuration JSON lié à l'application contient les règles métiers, calcul du nutriscore dans le code java à partir des règles récupérées depuis le fichier de ressource JSON.
    * Exemple de fichier de configuration JSON: 
    ```json
    {
  "configVersions":[
    {
      "version":1,
      "nutrients":[
        {
          "name":"energy_100g",
          "factor":-1,
          "thresholds":[
            {
              "threshold":0,
              "score":0
            },
            {
              "threshold":335,
              "score":1
            },
            {
              "threshold":670,
              "score":2
            },
            {
              "threshold":1005,
              "score":3
            },
            {
              "threshold":1340,
              "score":4
            },
            {
              "threshold":1675,
              "score":5
            },
            {
              "threshold":2010,
              "score":6
            },
            {
              "threshold":2345,
              "score":7
            },
            {
              "threshold":2680,
              "score":8
            },
            {
              "threshold":3015,
              "score":9
            },
            {
              "threshold":3350,
              "score":10
            }
          ]
        },
        {
          "name":"satured-fat_100g",
          "factor":-1,
          "thresholds":[
            {
              "threshold":0,
              "score":0
            },
        etc ...
    ```
* Base de données : Une entité (table) permet de stocker les règles métiers, calcul du nutriscore dans le code java à partir des règles récupérées depuis le fichier data.sql chargé automatiquement par spring dans une base de données H2 (RAM) au démarrage de l'application.
    * Exemple de fichier data.sql: 
    ```sql
    insert into rule(id, name, points, min_bound, component) values(1, 'energy_100g', 0, 0, 'N');
    insert into rule(id, name, points, min_bound, component) values(2, 'energy_100g', 1, 335, 'N');
    insert into rule(id, name, points, min_bound, component) values(3, 'energy_100g', 2, 670, 'N');
    insert into rule(id, name, points, min_bound, component) values(4, 'energy_100g', 3, 1005, 'N');
    insert into rule(id, name, points, min_bound, component) values(5, 'energy_100g', 4, 1340, 'N');
    insert into rule(id, name, points, min_bound, component) values(6, 'energy_100g', 5, 1675, 'N');
    insert into rule(id, name, points, min_bound, component) values(7, 'energy_100g', 6, 2010, 'N');
    insert into rule(id, name, points, min_bound, component) values(8, 'energy_100g', 7, 2345, 'N');
    insert into rule(id, name, points, min_bound, component) values(9, 'energy_100g', 8, 2680, 'N');
    insert into rule(id, name, points, min_bound, component) values(10, 'energy_100g', 9, 3015, 'N');
    insert into rule(id, name, points, min_bound, component) values(11, 'energy_100g', 10, 3350, 'N');
    ```
* Calcul via une requête JPQL : Une entité (table) permet de stocker les règles métiers, calcul du nutriscore directement par une requête JPQL.
    * Exemple de requête JPQL: 
    ```sql
    select 
    sum(
    case r.component
        when 'N' then r.points 
        when 'P' then -r.points 
    end) as pts
    from rule r
    where name || min_bound in (select name || max(min_bound) as min_bound from rule 
            where (name = 'energy_100g' and min_bound <= 1962)
            or    (name = 'saturated-fat_100g' and min_bound <= 5.6)
            or    (name = 'salt_100g' and min_bound <= 0.49)
            or    (name = 'sugars_100g' and min_bound <= 32)
            or    (name = 'fiber_100g' and min_bound <= 4)
            or    (name = 'proteins_100g' and min_bound <= 6.3)
            group by name);
    ```

Finalement, nous avons gardé la deuxième solution, stocker les règles en base de données semble être ce qui est le plus facilement modulable et maintenable. Le calcul à partir des règles récupérées sera fait en java pour des raisons de lisibilité du code (la requête JPQL est difficilement lisible et adaptable).

En ce qui concerne la gestion des additifs, nous avons décidé de centraliser toutes les règles métiers dans la base de données H2. Nous avons donc écrit un script afin de convertir le fichier CSV des additifs en INSERT SQL afin de les insérer automatiquement dans la base de données via le fichier data.sql.
Exemple d'ajout d'additifs : 
```sql
INSERT INTO additive(tag, name, dangerousness) VALUES ('E100', 'Curcumines, Jaune de curcumine', 'Ne pas abuser');
INSERT INTO additive(tag, name, dangerousness) VALUES ('E101', 'Riboflavine, Riboflavine-5-phosphate, Vitamine B2', 'Ne pas abuser');
INSERT INTO additive(tag, name, dangerousness) VALUES ('E102', 'Tartrazine', 'Très toxique');
```

### 2 - Configuration de l'API

Nous avons créé une classe de configuration pour paramétrer les appels à l'API d'OpenFoodFacts. Cette classe créé un Bean de type RestTemplate contenant l'url de base de l'API et les entêtes http par défaut d'une requête. De plus, on charge dans cette classe l'adresse de base de l'API qui est reprise d'un fichier .properties. Cela nous permet de facilement modifier les accès à l'API, ce qui peut par exemple faciliter les passages d'un environnement de tests à un environnement de production.

ApiRestTemplateConfiguration :
```java
@Configuration
@PropertySource("classpath:caillou.properties")
public class ApiRestTemplateConfiguration {

    @Value("${openfoodfacts.api.baseurl}")
    private String apiBaseUrl;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .rootUri(apiBaseUrl)
                .defaultHeader("Content-Type", "application/json")
                .build();
    }
}
```

Fichier .properties : caillou.properties
```properties
openfoodfacts.api.baseurl=https://world.openfoodfacts.org/api/v0
```

Nous avons également versionné l'API, en préfixant les routes de ``/v1/``. Cela nous permet de faire évoluer l'application en conservant plusieurs versions de l'API en parallèle.

### 3 - Persistance du panier

Dans un soucis de réalisme, où une personne quittant l'application ne souhaite pas perdre son panier, nous avons décidé de persister le panier.
Nous avons mis en place un ensemble de routes permettant de gérer le panier d'un utilisateur depuis une API REST. Le panier d'un utilisateur est identifié par l'adresse mail de l'utilisateur.
Dans le cadre de ce TP, le panier est sauvegardé dans la base de données H2. Dans la pratique, il serait préférable de'utiliser une base de données persistante, par exemple une base Postgres, Oracle ou MySQL.

### 4 - Tests des repositories

Nous nous sommes posés de nombreuses questions quant à la façon de tester les repositories. La première méthode consiste à tester en utilisant directement les règles paramétrées dans le fichier data.sql. Cette méthode présente l'inconvénient que les tests soient trop liés au paramétrage (si le paramétrage change, il se peut qu'il faille changer les tests). Il est préférable de découpler traitement et données afin de tester les requêtes indépendemment des données. Nous avons donc mocké des règles métiers spécifiques pour les tests des algorithmes de calcul du nutriscore et des classes des produits. 

### 5 - Intégration continue

Avec GitLab, nous avons mis en place une pipeline d'intégration continue. Grâce à celle-ci, nous exécutons les tests après chaque commit et vérifions la couverture de ces derniers. 

Pendant le stage de test, nous affichons le rapport généré par Jacoco dans les logs et nous récupérons le taux de couverture grâce à une regex depuis GitLab. Les packages dto et entities sont ignorés pour la couverture du code car les getters/setters, hashCode et toString sont générés automatiquement par Lombok. Nous partons donc du principe que ces fonctionnalités ont déjà été testées lors du développement de Lombok.

### 6 - Documentation automatique de l'API

Nous avons mis en place une documentation automatique de notre API à l'aide de Swagger (OpenAPI). Pour cela, nous avons inclu la dépendance suivante au projet :
```xml
<dependency>
    <groupId>org.springdoc</groupId>
    <artifactId>springdoc-openapi-ui</artifactId>
    <version>1.6.6</version>
</dependency>
```

Il n'y a pas d'autre démarche à réaliser, la simple inclusion de cette bibliothèque créé une documentation Swagger automatique de tous les controlleurs déclarés dans le projet.
Cette documentation est disponible à l'adresse suivante : ```http://localhost:8080/swagger-ui/index.html```
