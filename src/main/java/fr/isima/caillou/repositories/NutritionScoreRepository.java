package fr.isima.caillou.repositories;

import fr.isima.caillou.entities.NutritionScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface NutritionScoreRepository extends JpaRepository<NutritionScore, Integer> {
    @Query("SELECT n FROM NutritionScore n WHERE ?1 BETWEEN n.lowerBound AND n.upperBound")
    NutritionScore findByBounds(Integer value);
}
