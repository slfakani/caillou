package fr.isima.caillou.repositories;

import fr.isima.caillou.entities.Additive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdditivesRepository extends JpaRepository<Additive, String> {
    @Query("SELECT a FROM Additive a WHERE Lower(a.tag) IN ?1")
    List<Additive> findAdditivesByTag(List<String> tags);
}
