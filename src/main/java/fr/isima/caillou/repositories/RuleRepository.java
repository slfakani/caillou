package fr.isima.caillou.repositories;

import fr.isima.caillou.entities.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RuleRepository extends JpaRepository<Rule, Integer> {
    @Query("SELECT DISTINCT r.name FROM Rule r")
    List<String> findNutrients();

    @Query("SELECT r FROM Rule r WHERE r.name = ?1 " +
            "AND r.points = (SELECT MAX(r2.points) " +
                            "FROM Rule r2 " +
                            "WHERE r2.name = ?1 AND r2.minBound <= ?2)")
    Rule findRuleByNutrientAndValue(String nutrientName, Double value);
}
