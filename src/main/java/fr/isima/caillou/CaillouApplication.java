package fr.isima.caillou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class CaillouApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaillouApplication.class, args);
	}

}
