package fr.isima.caillou.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource("classpath:caillou.properties")
public class ApiRestTemplateConfiguration {

    @Value("${openfoodfacts.api.baseurl}")
    private String apiBaseUrl;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .rootUri(apiBaseUrl)
                .defaultHeader("Content-Type", "application/json")
                .build();
    }
}
