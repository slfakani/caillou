package fr.isima.caillou.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Additive {
    @Id
    private String tag;
    private String name;
    private String dangerousness;
}
