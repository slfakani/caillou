package fr.isima.caillou.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Basket {
    @Id
    private String email;

    @OneToMany(cascade = CascadeType.ALL)
    private List<BasketProduct> products;

    @Entity
    @Data
    public static class BasketProduct {
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        private long id;
        @Column(name = "ean")
        private String ean;
        @Column(name = "quantity")
        private int quantity;

        public void addQuantity(int q)
        {
            quantity += q;
        }

        public boolean removeQuantity(int q)
        {
            quantity -= q;
            return quantity <= 0 ? true : false;
        }
    }
}
