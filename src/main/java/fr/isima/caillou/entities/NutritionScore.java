package fr.isima.caillou.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NutritionScore {
    @Id
    private Integer id;
    @Column(name = "classe")
    private String nutritionalClass;
    @Column(name = "lower_bound")
    private Integer lowerBound;
    @Column(name = "upper_bound")
    private Integer upperBound;
    private String color;
}
