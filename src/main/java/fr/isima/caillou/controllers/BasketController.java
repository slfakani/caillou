package fr.isima.caillou.controllers;

import fr.isima.caillou.dto.BasketProductDto;
import fr.isima.caillou.dto.BasketResponse;
import fr.isima.caillou.entities.Basket;
import fr.isima.caillou.exceptions.BasketNotFoundException;
import fr.isima.caillou.repositories.BasketRepository;
import fr.isima.caillou.services.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/basket")
public class BasketController {
    @Autowired
    private BasketRepository basketRepository;
    @Autowired
    private BasketService basketService;

    @GetMapping(value = "/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BasketResponse> getBasket(@PathVariable String email) {
        var response = basketService.getBasketWithNutriscore(email);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = "/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Basket> putProductListBasket(@PathVariable String email,
                                                       @RequestBody List<BasketProductDto> products) {
        var newProducts = BasketProductDto.toProductList(products);
        var basket = basketService.addBasketProductList(email, newProducts);
        return new ResponseEntity<>(basket, HttpStatus.CREATED);
    }

    @PostMapping(value = "/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Basket> postBasket(@PathVariable String email,
                                             @RequestBody List<BasketProductDto> products) {
        var newProducts = BasketProductDto.toProductList(products);
        var basket = basketService.replaceBasketList(email, newProducts);
        return new ResponseEntity<>(basket, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{email}")
    public ResponseEntity deleteBasket(@PathVariable String email) throws BasketNotFoundException {
        try {
            basketRepository.deleteById(email);
        }
        catch (EmptyResultDataAccessException e){
            throw new BasketNotFoundException();
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/products/{email}")
    public ResponseEntity deleteProductListBasket(@PathVariable String email,
                                                  @RequestBody List<Basket.BasketProduct> products)
            throws BasketNotFoundException {
        basketService.removeBasketProductList(email, products);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
