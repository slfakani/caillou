package fr.isima.caillou.controllers;

import fr.isima.caillou.exceptions.ProductNotFoundException;
import fr.isima.caillou.services.ProductConstructionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.UnknownHostException;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductConstructionService productConstructionService;

    @GetMapping(value = "/{ean}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getProduct(@PathVariable String ean) throws ProductNotFoundException {
        var response = productConstructionService.getProductResponse(ean);
        return ResponseEntity.ok(response);
    }
    
    @ExceptionHandler({ UnknownHostException.class })
    public String handleUnknownHostException() {
        return "Network error";
    }
}
