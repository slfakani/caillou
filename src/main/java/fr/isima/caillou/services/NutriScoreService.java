package fr.isima.caillou.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.isima.caillou.dto.Product;
import fr.isima.caillou.repositories.RuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NutriScoreService {

    @Autowired
    private RuleRepository ruleRepository;

    static double getNutrientFromProduct(String nutrientName, Product product) {
        var mapper = new ObjectMapper();
        return mapper.convertValue(product, JsonNode.class).get(nutrientName).doubleValue();
    }

    public List<String> getNutrientsNames() {
        return ruleRepository.findNutrients();
    }

    public int getNutriScore(Product product) {
        if (product == null) throw new IllegalArgumentException();
        int score = 0;
        for(var nutrient : ruleRepository.findNutrients()) {
            var nutritionalValue = getNutrientFromProduct(nutrient, product);
            var validRule = ruleRepository.findRuleByNutrientAndValue(nutrient, nutritionalValue);
            score += (validRule.getComponent().equals("N") ? 1 : -1) * validRule.getPoints();
        }
        return score;
    }
}
