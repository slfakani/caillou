package fr.isima.caillou.services;

import fr.isima.caillou.dto.ProductResponse;
import fr.isima.caillou.repositories.AdditivesRepository;
import fr.isima.caillou.repositories.NutritionScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ProductConstructionService {

    @Autowired
    private NutriScoreService nutriScoreService;

    @Autowired
    private NutritionScoreRepository nutritionScoreRepository;

    @Autowired
    private OpenFoodFactsApiService openFoodFactsApiService;

    @Autowired
    private AdditivesRepository additivesRepository;

    public ProductResponse getProductResponse(String ean) {
        var openFoodFactApiResponse = openFoodFactsApiService.getProduct(ean);
        var product = openFoodFactApiResponse.getProduct();
        var nutritionScore = nutriScoreService.getNutriScore(product);
        var productClass = nutritionScoreRepository.findByBounds(nutritionScore);
        var additives = additivesRepository.findAdditivesByTag(
            product.getAdditivesTags().stream().map(t -> t.substring(3)).collect(Collectors.toList()));
        return new ProductResponse(
                0,
                product.getId(),
                product.getGenericName(),
                nutritionScore,
                productClass.getNutritionalClass(),
                productClass.getColor(),
                additives);
    }
}
