package fr.isima.caillou.services;

import fr.isima.caillou.dto.BasketResponse;
import fr.isima.caillou.dto.Product;
import fr.isima.caillou.entities.Basket;
import fr.isima.caillou.entities.NutritionScore;
import fr.isima.caillou.exceptions.BasketNotFoundException;
import fr.isima.caillou.repositories.BasketRepository;
import fr.isima.caillou.repositories.NutritionScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BasketService {

    @Autowired
    private BasketRepository basketRepository;

    @Autowired
    private NutriScoreService nutriscoreService;

    @Autowired
    private NutritionScoreRepository nutritionScoreRepository;

    @Autowired
    private OpenFoodFactsApiService openFoodFactsApiService;

    public BasketResponse getBasketWithNutriscore(String email) throws BasketNotFoundException {
        Optional<Basket> basket = basketRepository.findById(email);

        if (basket.isPresent()) {
            BasketResponse basketResponse = new BasketResponse();
            basketResponse.setEmail(basket.get().getEmail());
            basketResponse.setProducts(basket.get().getProducts());

            double nutriscore = 0.0;
            int nbProducts = 0;
            for (Basket.BasketProduct basketProduct : basket.get().getProducts()) {
                Product completeProduct = openFoodFactsApiService.getProduct(basketProduct.getEan()).getProduct();
                nutriscore += nutriscoreService.getNutriScore(completeProduct) * basketProduct.getQuantity();
                nbProducts += basketProduct.getQuantity();
            }
            nutriscore /= nbProducts;

            basketResponse.setNutriscore(nutriscore);
            NutritionScore nutritionScore = nutritionScoreRepository.findByBounds((int)nutriscore);
            basketResponse.setClasse(nutritionScore.getNutritionalClass());
            basketResponse.setColor(nutritionScore.getColor());

            return basketResponse;
        } else {
            throw new BasketNotFoundException();
        }
    }

    public Basket addBasketProductList(String email, List<Basket.BasketProduct> products) {
        Optional<Basket> basketSearch = basketRepository.findById(email);
        Basket basketInsert = new Basket();

        basketInsert = insertBasket(email, products, basketSearch, basketInsert);

        return basketRepository.save(basketInsert);
    }


    protected Basket insertBasket(String email,
                                  List<Basket.BasketProduct> products,
                                  Optional<Basket> basketSearch,
                                  Basket basketInsert) {
        if (basketSearch.isPresent()) {
            for (Basket.BasketProduct p : products) {
                Optional<Basket.BasketProduct> product = basketSearch
                        .get()
                        .getProducts()
                        .stream()
                        .filter(x -> x.getEan().equals(p.getEan()))
                        .findFirst();
                if (product.isPresent()) {
                    product.get().addQuantity(p.getQuantity());
                } else {
                    basketSearch.get().getProducts().add(p);
                }
            }
            basketInsert = basketSearch.get();
        } else {
            basketInsert.setEmail(email);
            basketInsert.setProducts(products);
        }
        return basketInsert;
    }

    public Basket replaceBasketList(String email, List<Basket.BasketProduct> products) {
        Basket basketInsert = new Basket();
        basketInsert.setEmail(email);
        basketInsert.setProducts(products);
        return basketRepository.save(basketInsert);
    }

    public void removeBasketProductList(String email, List<Basket.BasketProduct> products)
            throws BasketNotFoundException {
        Optional<Basket> basketSearch = basketRepository.findById(email);
        Basket basketInsert = new Basket();
        basketInsert.setEmail(email);
        basketInsert = removeFromBasket(products, basketSearch);
        basketRepository.save(basketInsert);
    }

    protected Basket removeFromBasket(List<Basket.BasketProduct> products, Optional<Basket> basketSearch) {
        Basket basketInsert;
        if (basketSearch.isPresent()) {
            for (Basket.BasketProduct p : products) {
                Optional<Basket.BasketProduct> product = basketSearch
                        .get()
                        .getProducts()
                        .stream()
                        .filter(x -> x.getEan().equals(p.getEan()))
                        .findFirst();
                product.ifPresent(basketProduct -> basketSearch.get().getProducts().remove(basketProduct));
            }
            basketInsert = basketSearch.get();
        }
        else {
            throw new BasketNotFoundException();
        }
        return basketInsert;
    }
}
