package fr.isima.caillou.services;

import fr.isima.caillou.dto.OpenFoodFactsApiResponse;
import fr.isima.caillou.exceptions.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OpenFoodFactsApiService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private NutriScoreService nutriScoreService;

    public OpenFoodFactsApiResponse getProduct(String id) throws ProductNotFoundException {
        var nutrientList = String.join(",", nutriScoreService.getNutrientsNames());
        var apiResponse = restTemplate.getForObject(
                String.format("/product/%s.json?fields=_id,generic_name,additives_tags,%s", id, nutrientList),
                OpenFoodFactsApiResponse.class);
        if (apiResponse == null || apiResponse.getStatus() == 0) {
            throw new ProductNotFoundException("Product not found");
        }
        return apiResponse;
    }
}
