package fr.isima.caillou.dto;

import fr.isima.caillou.entities.Additive;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {
    private long id;
    private String barCode;
    private String name;
    private int nutritionScore;
    private String classe;
    private String color;
    private List<Additive> additives;
}
