package fr.isima.caillou.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class OpenFoodFactsApiResponse {

    private String code;
    private Product product;
    private int status;
    @JsonProperty("status_verbose")
    private String statusVerbose;
}
