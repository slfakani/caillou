package fr.isima.caillou.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @JsonProperty("_id")
    private String id;
    @JsonProperty("generic_name")
    private String genericName;
    @JsonProperty("additives_tags")
    @NonNull
    private List<String> additivesTags = new ArrayList<>();
    @JsonProperty("energy_100g")
    private double energy100g;
    @JsonProperty("saturated-fat_100g")
    private double saturatedFat100g;
    @JsonProperty("sugars_100g")
    private double sugars100g;
    @JsonProperty("salt_100g")
    private double salt100g;
    @JsonProperty("fiber_100g")
    private double fiber100g;
    @JsonProperty("proteins_100g")
    private double proteins100g;
}
