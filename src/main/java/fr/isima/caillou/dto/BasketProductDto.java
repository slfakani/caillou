package fr.isima.caillou.dto;

import fr.isima.caillou.entities.Basket;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class BasketProductDto {
    private String ean;
    private int quantity;

    public static List<Basket.BasketProduct> toProductList(List<BasketProductDto> productDtoList) {
        return productDtoList.stream().map(p -> {
            var product = new Basket.BasketProduct();
            product.setEan(p.getEan());
            product.setQuantity(p.getQuantity());
            return product;
        }).collect(Collectors.toList());
    }
}
