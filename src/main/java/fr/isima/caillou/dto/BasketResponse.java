package fr.isima.caillou.dto;

import lombok.Data;

import java.util.List;

@Data
public class BasketResponse {

    private String email;
    private List<fr.isima.caillou.entities.Basket.BasketProduct> products;
    private Double nutriscore;
    private String classe;
    private String color;

    @Data
    public static class BasketProduct {

        private String id;
        private int quantity;
    }
}
