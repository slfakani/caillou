package fr.isima.caillou.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class BasketNotFoundException extends RuntimeException {
    public BasketNotFoundException() {
        super();
    }

    public BasketNotFoundException(String message) {
        super(message);
    }
}
