package fr.isima.caillou.controllers;

import fr.isima.caillou.dto.BasketResponse;
import fr.isima.caillou.entities.Basket;
import fr.isima.caillou.exceptions.BasketNotFoundException;
import fr.isima.caillou.repositories.BasketRepository;
import fr.isima.caillou.services.BasketService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(BasketController.class)
class BasketControllerTest {
    @MockBean
    private BasketService basketService;

    @MockBean
    private BasketRepository basketRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUpBasketService() {
        ArrayList<Basket.BasketProduct> products = new ArrayList<>();
        Basket.BasketProduct p1 = new Basket.BasketProduct();
        p1.setEan("123456");
        p1.setId(1);
        p1.setQuantity(5);
        Basket.BasketProduct p2 = new Basket.BasketProduct();
        p2.setEan("1234567");
        p2.setId(2);
        p2.setQuantity(10);

        products.add(p1);
        products.add(p2);

        BasketResponse basketResponse = new BasketResponse();
        basketResponse.setEmail("test@example.com");
        basketResponse.setNutriscore(18.0);
        basketResponse.setColor("red");
        basketResponse.setClasse("Degeu");
        basketResponse.setProducts(products);

        when(basketService.getBasketWithNutriscore("test@example.com"))
                .thenReturn(basketResponse);
        when(basketService.getBasketWithNutriscore("inexistant@example.com"))
                .thenThrow(new BasketNotFoundException());

        doThrow(new EmptyResultDataAccessException(0))
                .when(basketRepository)
                .deleteById("inexistant@example.com");

        doThrow(new EmptyResultDataAccessException(0))
                .when(basketRepository)
                .deleteById("inexistant@example.com/products");

        doThrow(new BasketNotFoundException())
                .when(basketService)
                .removeBasketProductList(eq("inexistant@example.com"), any());
    }

    @Test
    void get_givenEmail_whenEmailNotExist_thenReturn404() throws Exception {
        mockMvc.perform(get("/api/v1/basket/inexistant@example.com"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void get_givenEmail_whenEmailExist_thenReturn200() throws Exception {
        mockMvc.perform(get("/api/v1/basket/test@example.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is("test@example.com")))
                .andExpect(jsonPath("$.nutriscore", is(18.0)))
                .andExpect(jsonPath("$.classe", is("Degeu")))
                .andExpect(jsonPath("$.color", is("red")));
    }

    @Test
    void put_givenEmailAndProductList_whenEmailExist_thenReturn201() throws Exception {
        mockMvc.perform(put("/api/v1/basket/inexistant@example.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[\n" +
                        "    {\n" +
                        "        \"ean\":\"7622210449283\",\n" +
                        "        \"quantity\": 100\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"ean\":\"3274080005003\",\n" +
                        "        \"quantity\": 1\n" +
                        "    }\n" +
                        "]"))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void put_givenEmailAndProductList_whenEmailNotExist_thenReturn201() throws Exception {
        mockMvc.perform(put("/api/v1/basket/test@example.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[\n" +
                        "    {\n" +
                        "        \"ean\":\"7622210449283\",\n" +
                        "        \"quantity\": 100\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"ean\":\"3274080005003\",\n" +
                        "        \"quantity\": 1\n" +
                        "    }\n" +
                        "]"))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void post_givenEmailAndProductList_whenEmailExist_thenReturn201() throws Exception {
        mockMvc.perform(post("/api/v1/basket/inexistant@example.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[\n" +
                        "    {\n" +
                        "        \"ean\":\"7622210449283\",\n" +
                        "        \"quantity\": 100\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"ean\":\"3274080005003\",\n" +
                        "        \"quantity\": 1\n" +
                        "    }\n" +
                        "]"))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void post_givenEmailAndProductList_whenEmailNotExist_thenReturn201() throws Exception {
        mockMvc.perform(post("/api/v1/basket/test@example.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[\n" +
                        "    {\n" +
                        "        \"ean\":\"7622210449283\",\n" +
                        "        \"quantity\": 100\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"ean\":\"3274080005003\",\n" +
                        "        \"quantity\": 1\n" +
                        "    }\n" +
                        "]"))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void delete_givenEmail_whenEmailNotExist_thenReturn404() throws Exception {
        mockMvc.perform(delete("/api/v1/basket/inexistant@example.com"))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete_givenEmail_whenEmailExist_thenReturn204() throws Exception {
        mockMvc.perform(delete("/api/v1/basket/test@example.com"))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteProducts_givenEmail_whenEmailNotExist_thenReturn404() throws Exception {
        mockMvc.perform(delete("/api/v1/basket/products/inexistant@example.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[\n\n]]"))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteProducts_givenEmail_whenEmailExist_thenReturn204() throws Exception {
        mockMvc.perform(delete("/api/v1/basket/products/test@example.com").contentType(MediaType.APPLICATION_JSON)
                .content("[\n\n]]"))
                .andExpect(status().isNoContent());
    }
}
