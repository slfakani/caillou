package fr.isima.caillou.controllers;

import fr.isima.caillou.dto.ProductResponse;
import fr.isima.caillou.entities.Additive;
import fr.isima.caillou.exceptions.ProductNotFoundException;
import fr.isima.caillou.services.ProductConstructionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.net.UnknownHostException;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(MockitoExtension.class)
@WebMvcTest(ProductController.class)
class ProductControllerTest {

    @MockBean
    private ProductConstructionService productConstructionService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void givenProductResponse_whenProductExists_thenShouldReturnValidJson() throws Exception {
        when(productConstructionService.getProductResponse("7622210449283"))
            .thenReturn(new ProductResponse(1, "7622210449283",
                    "Chocapics",10,
                    "Mauvais", "Yellow",
                    List.of(new Additive("E150d", "Colorant Caramel", "Toxique"))));

        mockMvc.perform(get("/api/v1/products/7622210449283"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.barCode", is("7622210449283")))
                .andExpect(jsonPath("$.name", is("Chocapics")))
                .andExpect(jsonPath("$.color", is("Yellow")))
                .andExpect(jsonPath("$.classe", is("Mauvais")))
                .andExpect(jsonPath("$.additives[0].tag", is("E150d")))
                .andExpect(jsonPath("$.additives[0].name", is("Colorant Caramel")))
                .andExpect(jsonPath("$.additives[0].dangerousness", is("Toxique")));
    }

    @Test
    void givenProductResponse_whenProductDoesntExist_thenShouldReturn404() throws Exception {
        when(productConstructionService.getProductResponse("7311532515511"))
                .thenThrow(new ProductNotFoundException("Product not found"));

        mockMvc.perform(get("/api/v1/products/7311532515511"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void givenProductService_whenProductServiceThrowsHostException_thenShouldReturnErrorMessage() throws Exception {
        given(productConstructionService.getProductResponse("7311532515511"))
                .willAnswer( invocation -> { throw new UnknownHostException(); });

        mockMvc.perform(get("/api/v1/products/7311532515511"))
                .andDo(print())
                .andExpect(content().string(containsString("Network error")));
    }
}