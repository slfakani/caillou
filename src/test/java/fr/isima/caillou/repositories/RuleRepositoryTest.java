package fr.isima.caillou.repositories;

import fr.isima.caillou.entities.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql(statements = {
        "DELETE FROM Rule"
})
class RuleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RuleRepository ruleRepository;

    @BeforeEach
    void setUp() {
        entityManager.persist(new Rule(1, "energy_100g", 1, 0.0, "N"));
        entityManager.persist(new Rule(2, "energy_100g", 2, 100.0, "N"));
        entityManager.persist(new Rule(3, "energy_100g", 3, 200.0, "N"));
        entityManager.persist(new Rule(4, "energy_100g", 4, 300.0, "N"));
        entityManager.persist(new Rule(5, "energy_100g", 5, 400.0, "N"));

        entityManager.persist(new Rule(6, "sugars_100g", 1, 0.0, "N"));
        entityManager.persist(new Rule(7, "sugars_100g", 2, 40.0, "N"));
        entityManager.persist(new Rule(8, "sugars_100g", 3, 80.0, "N"));
        entityManager.persist(new Rule(9, "sugars_100g", 4, 120.0, "N"));
        entityManager.persist(new Rule(10, "sugars_100g", 5, 160.0, "N"));

        entityManager.persist(new Rule(11, "fibers_100g", 1, 0.0, "P"));
        entityManager.persist(new Rule(12, "fibers_100g", 2, 1.0, "P"));
        entityManager.persist(new Rule(13, "fibers_100g", 3, 2.0, "P"));
        entityManager.persist(new Rule(14, "fibers_100g", 4, 3.0, "P"));
        entityManager.persist(new Rule(15, "fibers_100g", 5, 4.0, "P"));
    }

    @Test
    void givenRulesInDatabase_whenFindNutrientIsCalled_thenItShouldReturnValidList() {
        assertEquals(ruleRepository.findNutrients(), List.of("energy_100g", "fibers_100g", "sugars_100g"));
    }

    @Test

    void givenRulesInDatabase_whenNutrientIsNegative_thenRuleShouldBeNull() {
        assertNull(ruleRepository.findRuleByNutrientAndValue("energy_100g", -1.0));
    }

    @Test
    void givenRulesInDatabase_whenNutrientDoesNotExist_thenRuleShouldBeNull() {
        assertNull(ruleRepository.findRuleByNutrientAndValue("", 1.0));
        assertNull(ruleRepository.findRuleByNutrientAndValue(null, 1.0));
        assertNull(ruleRepository.findRuleByNutrientAndValue("Does not exist", 1.0));
    }

    @Test
    void givenRulesInDatabase_whenNutrientExist_thenRuleShouldBeValid() {
        assertEquals(1.0,
                ruleRepository.findRuleByNutrientAndValue("energy_100g", 0.0).getPoints().doubleValue());
        assertEquals(2.0,
                ruleRepository.findRuleByNutrientAndValue("energy_100g", 100.0).getPoints().doubleValue());
        assertEquals(5.0,
                ruleRepository.findRuleByNutrientAndValue("energy_100g", 400.0).getPoints().doubleValue());
        assertEquals(5.0,
                ruleRepository.findRuleByNutrientAndValue("energy_100g", Double.MAX_VALUE).getPoints().doubleValue());
        assertEquals(3,
                ruleRepository.findRuleByNutrientAndValue("sugars_100g", 85.5).getPoints().doubleValue());
    }
}
