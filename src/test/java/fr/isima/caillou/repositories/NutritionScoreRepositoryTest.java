package fr.isima.caillou.repositories;

import fr.isima.caillou.entities.NutritionScore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql(statements = {
        "DELETE FROM nutrition_score" // Delete the tuples from data.sql in order to mock the db
})
class NutritionScoreRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private NutritionScoreRepository nutritionScoreRepository;

    @BeforeEach
    void setupMockDatabase() {
        entityManager.persist(
                new NutritionScore(1, "Bon", -10, -1, "Vert"));
        entityManager.persist(
                new NutritionScore(2, "Moyen", 0, 10, "Jaune"));
        entityManager.persist(
                new NutritionScore(3, "Mauvais", 11, 20, "Rouge"));
    }

    @Test
    void givenNutritionScoresInDatabase_whenValueIsOutOfBounds_thenReturnShouldBeNull() {
        assertNull(nutritionScoreRepository.findByBounds(Integer.MIN_VALUE));
        assertNull(nutritionScoreRepository.findByBounds(-1000));
        assertNull(nutritionScoreRepository.findByBounds(1000));
        assertNull(nutritionScoreRepository.findByBounds(Integer.MAX_VALUE));
    }

    @Test
    void givenNutritionScoresInDatabase_whenValueIsInUpperTier_thenResultShouldBeGood() {
        assertEquals(nutritionScoreRepository.findByBounds(-10).getNutritionalClass(), "Bon");
        assertEquals(nutritionScoreRepository.findByBounds(-9).getNutritionalClass(), "Bon");
        assertEquals(nutritionScoreRepository.findByBounds(-1).getNutritionalClass(), "Bon");
    }

    @Test
    void givenNutritionScoresInDatabase_whenValueIsInMiddleTier_thenResultShouldBeAverage() {
        assertEquals(nutritionScoreRepository.findByBounds(0).getNutritionalClass(), "Moyen");
        assertEquals(nutritionScoreRepository.findByBounds(5).getNutritionalClass(), "Moyen");
        assertEquals(nutritionScoreRepository.findByBounds(10).getNutritionalClass(), "Moyen");
    }

    @Test
    void givenNutritionScoresInDatabase_whenValueIsInLowerTier_thenResultShouldBeBad() {
        assertEquals(nutritionScoreRepository.findByBounds(11).getNutritionalClass(), "Mauvais");
        assertEquals(nutritionScoreRepository.findByBounds(16).getNutritionalClass(), "Mauvais");
        assertEquals(nutritionScoreRepository.findByBounds(20).getNutritionalClass(), "Mauvais");
    }
}