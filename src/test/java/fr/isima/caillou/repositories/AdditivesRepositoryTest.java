package fr.isima.caillou.repositories;

import fr.isima.caillou.entities.Additive;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql(statements = { "DELETE FROM additive" }) // Delete tuples inserted by data.sql in order to mock the db and test the repository
class AdditivesRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AdditivesRepository additivesRepository;

    @BeforeEach
    void setUp() {
        entityManager.persist(
                new Additive("E101", "Riboflavine", ""));
        entityManager.persist(
                new Additive("E150Dd", "Colorant Caramel", "Toxique"));
        entityManager.persist(
                new Additive("E521", "Sulfate d'aluminium sodique", "Très Toxique"));
    }

    @Test
    void givenAdditivesRepository_whenTagListIsEmpty_thenAdditiveListShouldBeEmpty() {
        assertEquals(additivesRepository.findAdditivesByTag(List.of()).size(), 0);
    }

    @Test
    void givenAdditivesRepository_whenTagListHasNonExistingValues_thenAdditiveListShouldBeEmpty() {
        assertEquals(additivesRepository.findAdditivesByTag(List.of("e102", "e404")).size(), 0);
    }

    @Test
    void givenAdditivesRepository_whenTagListHasNonExistingValuesAndExistingValues_thenAdditiveListShouldNotBeEmpty() {
        var additives = additivesRepository
                .findAdditivesByTag(List.of("e101", "e202", "e303", "e521", "e661"));
        assertEquals(additives.size(), 2);
        assertEquals(additives.get(0).getTag(), "E101");
        assertEquals(additives.get(0).getName(), "Riboflavine");
        assertEquals(additives.get(0).getDangerousness(), "");
        assertEquals(additives.get(1).getTag(), "E521");
        assertEquals(additives.get(1).getName(), "Sulfate d'aluminium sodique");
        assertEquals(additives.get(1).getDangerousness(), "Très Toxique");
    }
}