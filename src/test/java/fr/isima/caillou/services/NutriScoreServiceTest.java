package fr.isima.caillou.services;

import fr.isima.caillou.dto.Product;
import fr.isima.caillou.entities.Rule;
import fr.isima.caillou.repositories.RuleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DataJpaTest
@Sql(statements = { "DELETE FROM Rule" })
class NutriScoreServiceTest {

    @Mock
    private RuleRepository ruleRepository;

    @InjectMocks
    private NutriScoreService nutriScoreService;

    @BeforeEach
    void setUp() {
        when(ruleRepository.findNutrients())
            .thenReturn(List.of("energy_100g", "sugars_100g", "proteins_100g"));
        when(ruleRepository.findRuleByNutrientAndValue(eq("energy_100g"), doubleThat(v -> v < 100.0)))
            .thenReturn(new Rule(1, "energy_100g", 0, 0.0, "N"));
        when(ruleRepository.findRuleByNutrientAndValue(eq("sugars_100g"), doubleThat(v -> v < 4.5)))
            .thenReturn(new Rule(2, "sugars_100g", 0, 0.0, "N"));
        when(ruleRepository.findRuleByNutrientAndValue(eq("proteins_100g"), doubleThat(v -> v < 1.6)))
            .thenReturn(new Rule(3, "proteins_100g", 0, 0.0, "P"));

        when(ruleRepository.findRuleByNutrientAndValue(eq("energy_100g"), doubleThat(v -> v >= 100 && v < 200.0)))
            .thenReturn(new Rule(4, "energy_100g", 1, 100.0, "N"));
        when(ruleRepository.findRuleByNutrientAndValue(eq("sugars_100g"), doubleThat(v -> v >= 4.5 && v < 9)))
            .thenReturn(new Rule(5, "sugars_100g", 1, 4.5, "N"));
        when(ruleRepository.findRuleByNutrientAndValue(eq("proteins_100g"), doubleThat(v -> v >= 1.6 && v < 3.2)))
            .thenReturn(new Rule(5, "proteins_100g", 1, 1.6, "P"));
    }

    @Test
    void givenValidRules_whenNutrientsAreNegative_thenNutriscoreShouldBeZero() {
        assertEquals(0, nutriScoreService.getNutriScore(
                        new Product("234", "BigMac", List.of(),
                                -10.0, -30.0,
                                -10.0, -50.0,
                                -50.0, -30.0)));
    }

    @Test
    void givenValidRules_whenNutrientsAreZero_thenNutriscoreShouldBeZero() {
        assertEquals(0, nutriScoreService.getNutriScore(
                new Product("391", "Colle UHU", List.of(),
                        0.0, 0.0,
                        0.0, 0.0,
                        0.0, 0.0)));
    }

    @Test
    void givenValidRules_whenNutrientsAreAllNegative_thenNutriscoreShouldBePositive() {
        assertEquals(2, nutriScoreService.getNutriScore(
                new Product("6931", "Coca Light", List.of(),
                        150.0, 0.0,
                        4.64, 0.0,
                        0.0, 0.1)));
    }

    @Test
    void givenValidRules_whenNutrientsAreAllPositive_thenNutriscoreShouldBeNegative() {
        assertEquals(-1, nutriScoreService.getNutriScore(
                new Product("991", "Coca Light", List.of(),
                        1.0, 0.0,
                        0.7, 0.0,
                        0.0, 3.1)));
    }

    @Test
    void givenValidRules_whenNutrientsAreMixed_thenNutriscoreShouldBeValid() {
        assertEquals(1, nutriScoreService.getNutriScore(
                new Product("831", "Coca Light", List.of(),
                        143.1, 0.0,
                        7, 0.0,
                        0.0, 3.1)));
    }

    @Test
    void givenValidRules_whenProductIsNull_thenShouldThrow() {
        assertThrows(IllegalArgumentException.class, () -> nutriScoreService.getNutriScore(null));
    }

    @Test
    void givenProduct_whenRetrievingNutrients_thenTheyShouldBeValid() {
        var product = new Product("15917", "Riz au lait", List.of(),
                49.3, 5.3, 11.1,
                1.4, 66.1, 7.2);
        assertEquals(NutriScoreService.getNutrientFromProduct("energy_100g", product), 49.3);
        assertEquals(NutriScoreService.getNutrientFromProduct("saturated-fat_100g", product), 5.3);
        assertEquals(NutriScoreService.getNutrientFromProduct("sugars_100g", product), 11.1);
        assertEquals(NutriScoreService.getNutrientFromProduct("salt_100g", product), 1.4);
        assertEquals(NutriScoreService.getNutrientFromProduct("fiber_100g", product), 66.1);
        assertEquals(NutriScoreService.getNutrientFromProduct("proteins_100g", product), 7.2);
    }

    @Test
    void givenValidRules_whenRetrievingNutrients_thenTheyShouldBeDefined() {
        assertEquals(nutriScoreService.getNutrientsNames(), List.of("energy_100g", "sugars_100g", "proteins_100g"));
    }
}
