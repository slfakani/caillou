package fr.isima.caillou.services;

import fr.isima.caillou.dto.OpenFoodFactsApiResponse;
import fr.isima.caillou.dto.Product;
import fr.isima.caillou.exceptions.ProductNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class OpenFoodFactsApiServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private NutriScoreService nutriScoreService;

    @InjectMocks
    private OpenFoodFactsApiService openFoodFactsApiService;

    @BeforeEach
    void setup() {
        var product = new Product("7622210449283", "Chocapics", List.of(),
                1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
        when(nutriScoreService.getNutrientsNames())
                .thenReturn(List.of("energy_100g", "saturated-fat_100g",
                        "sugars_100g", "salt_100g",
                        "fiber_100g", "proteins_100g"));
        when(restTemplate.getForObject(
                "/product/7622210449283.json?fields=_id,generic_name,additives_tags," +
                        "energy_100g,saturated-fat_100g,sugars_100g,salt_100g,fiber_100g,proteins_100g",
                OpenFoodFactsApiResponse.class))
                .thenReturn(new OpenFoodFactsApiResponse("7622210449283", product,
                        2, "product found"));
    }

    @Test
    void whenEanIsValid_thenProductShouldExist() {
        var response = openFoodFactsApiService.getProduct("7622210449283");
        assertNotNull(response);
        assertEquals(response.getCode(), "7622210449283");
        assertEquals(response.getProduct().getGenericName(), "Chocapics");
    }

    @Test
    void whenEanIsInvalid_thenShouldThrow() {
        assertThrows(ProductNotFoundException.class, () -> openFoodFactsApiService.getProduct("1"));
        assertThrows(ProductNotFoundException.class, () -> openFoodFactsApiService.getProduct(""));
        assertThrows(ProductNotFoundException.class, () -> openFoodFactsApiService.getProduct("string"));
    }

    @Test
    void whenEanIsNull_thenShouldThrow() {
        assertThrows(ProductNotFoundException.class, () -> openFoodFactsApiService.getProduct(null));
    }
}