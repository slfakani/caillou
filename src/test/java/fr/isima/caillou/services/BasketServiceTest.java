package fr.isima.caillou.services;

import fr.isima.caillou.dto.BasketResponse;
import fr.isima.caillou.dto.OpenFoodFactsApiResponse;
import fr.isima.caillou.dto.Product;
import fr.isima.caillou.entities.Basket;
import fr.isima.caillou.entities.NutritionScore;
import fr.isima.caillou.exceptions.BasketNotFoundException;
import fr.isima.caillou.repositories.BasketRepository;
import fr.isima.caillou.repositories.NutritionScoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.AdditionalAnswers.returnsFirstArg;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DataJpaTest
class BasketServiceTest {


    @Mock
    private BasketRepository basketRepository;

    @Mock
    private NutriScoreService nutriscoreService;

    @Mock
    private NutritionScoreRepository nutritionScoreRepository;

    @Mock
    private OpenFoodFactsApiService openFoodFactsApiService;

    @InjectMocks
    private BasketService basketService;

    private final Basket basket = new Basket();

    private final Basket.BasketProduct bp1 = new Basket.BasketProduct();
    private final Basket.BasketProduct bp2 = new Basket.BasketProduct();

    private final Product p1 = new Product();
    private final Product p2 = new Product();


    @BeforeEach
    void setUpNutritionScoreRepository() {
        NutritionScore nutritionScoreYellow = new NutritionScore();
        nutritionScoreYellow.setColor("yellow");
        nutritionScoreYellow.setNutritionalClass("Mangeable");

        NutritionScore nutritionScoreOrange = new NutritionScore();
        nutritionScoreOrange.setColor("orange");
        nutritionScoreOrange.setNutritionalClass("Mouais");

        NutritionScore nutritionScoreRed = new NutritionScore();
        nutritionScoreRed.setColor("red");
        nutritionScoreRed.setNutritionalClass("Degueu");

        when(nutritionScoreRepository.findByBounds(10))
                .thenReturn(nutritionScoreYellow);
        when(nutritionScoreRepository.findByBounds(17))
                .thenReturn(nutritionScoreOrange);
        when(nutritionScoreRepository.findByBounds(20))
                .thenReturn(nutritionScoreRed);
    }

    @BeforeEach
    void setUpOpenFoodFactsApiService() {
        bp1.setEan("1234567891012");
        bp1.setQuantity(4);

        bp2.setEan("9876543219873");
        bp2.setQuantity(12);

        p1.setGenericName("produit test 1");
        p1.setId(bp1.getEan());
        p1.setEnergy100g(1000);
        p1.setFiber100g(2);
        p1.setProteins100g(3.5);
        p1.setSalt100g(370);
        p1.setSaturatedFat100g(5);
        p1.setSugars100g(18);

        p2.setGenericName("produit test 2");
        p2.setId(bp2.getEan());
        p2.setEnergy100g(1250);
        p2.setFiber100g(3);
        p2.setProteins100g(4.5);
        p2.setSalt100g(470);
        p2.setSaturatedFat100g(2.3);
        p2.setSugars100g(15);

        OpenFoodFactsApiResponse op1 = new OpenFoodFactsApiResponse();
        op1.setCode(bp1.getEan());
        op1.setProduct(p1);

        OpenFoodFactsApiResponse op2 = new OpenFoodFactsApiResponse();
        op2.setCode(bp2.getEan());
        op2.setProduct(p2);

        when(openFoodFactsApiService.getProduct("1234567891012"))
                .thenReturn(op1);
        when(openFoodFactsApiService.getProduct("9876543219873"))
                .thenReturn(op2);
    }

    @BeforeEach
    void setUpNutriscoreService() {
        when(nutriscoreService.getNutriScore(p1))
                .thenReturn(10);
        when(nutriscoreService.getNutriScore(p2))
                .thenReturn(20);
    }

    @BeforeEach
    void setUpBasketRepository() {
        ArrayList<Basket.BasketProduct> products = new ArrayList<>();
        products.add(bp1);
        products.add(bp2);

        basket.setEmail("test@example.com");
        basket.setProducts(products);

        Optional<Basket> opt = Optional.of(basket);

        when(basketRepository.findById("test@example.com"))
                .thenReturn(opt);

        when(basketRepository.save(any()))
                .then(returnsFirstArg());
    }


    @Test
    void givenEmail_whenEmailNotExist_thenReturnNull() {
        assertThrows(BasketNotFoundException.class, () -> basketService.getBasketWithNutriscore("test2@example.com"));
    }

    @Test
    void givenEmail_whenEmailExist_thenReturnBasket() {
        BasketResponse response = new BasketResponse();
        response.setEmail(basket.getEmail());
        response.setColor("orange");
        response.setClasse("Mouais");
        response.setNutriscore(17.5);
        response.setProducts(basket.getProducts());
        assertEquals(response, basketService.getBasketWithNutriscore("test@example.com"));
    }

    @Test
    void givenEmailAndProductList_whenInsertingProducts_thenUpdateBasketWithProductList() {
        ArrayList<Basket.BasketProduct> products = new ArrayList<>();
        products.add(bp1);
        products.add(bp2);
        Optional<Basket> basketSearch = Optional.empty();
        Basket basketInsert = new Basket();

        basketService.insertBasket("testInsert@example.com", products, basketSearch, basketInsert);
        assertEquals(basketInsert.getProducts(), products);
        assertEquals(basketInsert.getEmail(), "testInsert@example.com");

        basketSearch = Optional.of(basketInsert);

        basketService.insertBasket("testInsert@example.com", products, basketSearch, basketInsert);
        assertEquals(basketInsert.getProducts().get(0).getEan(), bp1.getEan());
        assertEquals(basketInsert.getProducts().get(0).getQuantity(), 8);
        assertEquals(basketInsert.getProducts().get(1).getEan(), bp2.getEan());
        assertEquals(basketInsert.getProducts().get(1).getQuantity(), 24);
        assertEquals(basketInsert.getEmail(), "testInsert@example.com");
    }

    @Test
    void givenEmailAndProductList_whenRemovingProducts_thenUpdateBasketWithProductList() {
        ArrayList<Basket.BasketProduct> products = new ArrayList<>();
        products.add(bp1);
        products.add(bp2);
        Optional<Basket> basketSearch = Optional.empty();
        Basket basketInsert = new Basket();
        basketService.insertBasket("testInsert@example.com", products, basketSearch, basketInsert);
        basketSearch = Optional.of(basketInsert);

        ArrayList<Basket.BasketProduct> productsToRemove = new ArrayList<>();
        productsToRemove.add(bp1);

        basketInsert = basketService.removeFromBasket(productsToRemove, basketSearch);
        assertEquals(basketInsert.getProducts().size(), 1);
        assertEquals(basketInsert.getProducts().get(0).getEan(), bp2.getEan());
        assertEquals(basketInsert.getProducts().get(0).getQuantity(), 12);
        assertEquals(basketInsert.getEmail(), "testInsert@example.com");
    }

    @Test
    void givenEmailAndProductList_whenInserting_thenItShouldReturnUpdatedBasket() {
        Basket basket = basketService.addBasketProductList("test@example.com", List.of(bp1));
        assertEquals(basket.getEmail(), "test@example.com");
        assertEquals(basket.getProducts().size(), 2);
        assertEquals(basket.getProducts().get(0).getEan(), bp1.getEan());
        assertEquals(basket.getProducts().get(0).getQuantity(), 8);
        assertEquals(basket.getProducts().get(1).getEan(), bp2.getEan());
        assertEquals(basket.getProducts().get(1).getQuantity(), 12);
    }

    @Test
    void givenEmailAndProductList_whenReplacing_thenItShouldReturnReplacedBasket() {
        Basket basket = basketService.replaceBasketList("testReplace@example.com", List.of(bp1));
        assertEquals(basket.getEmail(), "testReplace@example.com");
        assertEquals(basket.getProducts().size(), 1);
        assertEquals(basket.getProducts().get(0).getEan(), bp1.getEan());
        assertEquals(basket.getProducts().get(0).getQuantity(), 4);
    }
}
