package fr.isima.caillou.services;

import fr.isima.caillou.dto.OpenFoodFactsApiResponse;
import fr.isima.caillou.dto.Product;
import fr.isima.caillou.entities.Additive;
import fr.isima.caillou.entities.NutritionScore;
import fr.isima.caillou.repositories.AdditivesRepository;
import fr.isima.caillou.repositories.NutritionScoreRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
class ProductConstructionServiceTest {

    @Mock
    private NutriScoreService nutriScoreService;

    @Mock
    private NutritionScoreRepository nutritionScoreRepository;

    @Mock
    private OpenFoodFactsApiService openFoodFactsApiService;

    @Mock
    private AdditivesRepository additivesRepository;

    @InjectMocks
    private ProductConstructionService productConstructionService;

    @Test
    void givenProduct_whenRetrievingResponse_thenItShouldBeDefined() {
        var product = new Product("7622210449283", "BMW M3 GTR",
                List.of("en:e150d", "en:e283"),
                1.3, 4.2, 7.2,
                4.1, 5.5, 9.1);
        when(openFoodFactsApiService.getProduct("7622210449283"))
            .thenReturn(new OpenFoodFactsApiResponse("7622210449283", product,
                    2, "product found"));
        when(nutriScoreService.getNutriScore(product))
            .thenReturn(10);
        when(nutritionScoreRepository.findByBounds(10))
            .thenReturn(new NutritionScore(1, "Mauvais",
                    5, 15, "rouge"));
        when(additivesRepository.findAdditivesByTag(List.of("e150d", "e283")))
            .thenReturn(List.of(
                    new Additive("E150D", "Colorant", "Toxique"),
                    new Additive("E283", "Propionate de potassium", "Douteux")));

        var productResponse = productConstructionService.getProductResponse("7622210449283");
        assertEquals(productResponse.getBarCode(), "7622210449283");
        assertEquals(productResponse.getName(), "BMW M3 GTR");
        assertEquals(productResponse.getClasse(), "Mauvais");
        assertEquals(productResponse.getNutritionScore(), 10);
    }
}